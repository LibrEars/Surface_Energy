# Surface Energy
This [jupyter notebook](https://nbviewer.org/urls/codeberg.org/LibrEars/Surface_Energy/raw/branch/master/Surface_Energy.ipynb) contains calculations to calculate the surface energy [1,2] of functional inks from measuring contact angles on two known substrates. The calculations were done for my bachelor and master thesis [3,4].

Note: There is an advanced plot including the possibility to also plot uncertainty for wetting envelops available in my newer python module [experiment_evaluation](https://codeberg.org/LibrEars/experiment_evaluation).

## References
[1] https://www.kruss-scientific.com/en-US/know-how/glossary/surface-free-energy

[2] Schlißke, S. Substratfunktionalisierungen zur Optimierung tintenstrahlgedruckter opto-elektronischer Bauteile. (2021). doi:10.5445/IR/1000130024.

[3] Seiberlich, M. Ultradünne dielektrische Elastomeraktuatoren für optische Anwendungen. Universitätsbibliothek Heidelberg (2017).

[4] Seiberlich, M. Herstellung gedruckter OLEDs auf Basis kleiner kupferbasierter Moleküle. Universitätsbibliothek Heidelberg (2015).


## GNU General Public License

Copyright (C) 2016-2023 M. Seiberlich 

Authors - M. Seiberlich


The code and documentation in this notebook is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

The notebook is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the notebook. If not, see <https://www.gnu.org/licenses/>. 
